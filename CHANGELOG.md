# Changelog

All notable changes to `livewire-gen` will be documented in this file

## 1.0.0 - 2021-08-07

- initial release
